// feather icons
feather.replace()

// make body long enough to move footer down
let navHeight = $("nav").outerHeight()
let footerHeight = $("footer").outerHeight();

// set content height so footer is at bottom
$(".main-content").css({
  "min-height": "calc(100vh - " + navHeight + "px - " + footerHeight + "px)"
});

// generic function to smoothly scroll to an ID
function scrollToID(id){
  $("html,body").animate({
   scrollTop: $("#" + id).offset().top
  }, "slow");
}