from django.db import models
from django.utils.text import slugify

class Country(models.Model):
    name = models.CharField(max_length=255)
    iso = models.CharField(max_length=2, db_index=True)
    def __str__(self):
        return self.name
    class Meta:
        verbose_name_plural = "Countries"
        ordering = ["name"]

class Map(models.Model):
    country = models.ForeignKey("Country", on_delete=models.CASCADE)
    svg = models.TextField()
    name = models.CharField(max_length=255)
    def __str__(self):
        return self.country.name + " - " + self.name

class Election(models.Model):
    name = models.CharField(max_length=255)
    slug = models.SlugField(max_length=255, null=False, unique=True)
    country = models.ForeignKey("Country", on_delete=models.CASCADE)
    map = models.ForeignKey("Map", on_delete=models.CASCADE)
    start_date = models.DateField(null=True)
    end_date = models.DateField(null=True)
    source = models.URLField(null=True, blank=True)
    candidates = models.FileField(null=True)
    votes = models.FileField(null=True)
    seats = models.FileField(null=True, blank=True)
    seat_winners = models.FileField(null=True, blank=True)
    max_percentage = models.IntegerField()
    zoom = models.ManyToManyField("Zoom", related_name="elections", blank=True)
    # candidate type options
    PERSON = "PE"
    PARTY = "PA"
    BOTH = "BO"
    CANDIDATE_TYPE = [
        (PERSON, "Person"),
        (PARTY, "Party"),
        (BOTH, "Both"),
    ]
    candidate_type = models.CharField(
        max_length=2,
        choices=CANDIDATE_TYPE,
        default=PARTY,
    )

    BLACK = "black"
    WHITE = "white"
    BORDER_COLOURS = [
        (BLACK, "Black"),
        (WHITE, "White"),
    ]
    border_colour = models.CharField(
        max_length=5,
        choices=BORDER_COLOURS,
        default=BLACK,
    )

    def save(self, *args, **kwargs):
        self.slug = slugify(self.name)
        super().save(*args, **kwargs)

    def __str__(self):
        return self.name
    class Meta:
        ordering = ["-start_date"]

class Zoom(models.Model):
    name = models.CharField(max_length=255)
    slug = models.CharField(max_length=255)
    viewbox = models.CharField(max_length=255)
    def __str__(self):
        return self.name