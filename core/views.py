import pandas as pd
from pandas import DataFrame, read_csv
from django.shortcuts import render, get_object_or_404, redirect
from django.http import Http404, HttpResponseRedirect
from .models import *
from django.template.defaulttags import register
from django.templatetags.static import static
from django.views.decorators.clickjacking import xframe_options_exempt

@register.filter
def get_item(dictionary, key):
    return dictionary.get(key)

def index(request):
    context = {
        "header": False,
        "footer": False,
        "title": False,
    }
    return render(request, "index.html", context)

def about(request):
    context = {
        "page_title": "About",
        "about": True,
    }
    return render(request, "about.html", context)

def start(request):
    context = {
        "page_title": "Get started",
        "start": True
    }
    return render(request, "start.html", context)

def elections(request):
    elections = Election.objects.order_by("-start_date", "-end_date")
    context = {
        "page_title": "Elections",
        "elections": elections,
        "election_list": True
    }
    return render(request, "elections.html", context)

@xframe_options_exempt
def election(request, slug):
    election = get_object_or_404(Election, slug=slug)

    # get election results from CSV file and read it
    results = pd.read_csv(election.votes)
    candidates = pd.read_csv(election.candidates)

    # add a total row for each candidate
    results.loc["party_total"] = results.sum(numeric_only=True)
    results.at["party_total", "constituency"] = election.country.name

    # add winner for each constituency
    results["winner"] = results.select_dtypes("number").idxmax(axis=1)

    # add a total column for each constituency
    results["constituency_total"] = results.sum(axis=1)

    # add percentages for each party in each constituency
    for candidate in candidates["slug"]:
        results[candidate + "_percent"] = ((results[candidate] / results["constituency_total"]) * 100).round(2)

    # make a dictionary of candidates
    candidates_dict = candidates.to_dict("records")

    # make a json object of the candidates
    candidates_json = candidates.to_json(orient="records")

    # make a dictionary of the results
    results_dict = results.to_dict("records")

    # make a json object of the results
    results_json = results.to_json(orient="records")

    # add seats, if available
    seats_dict = ""
    seat_winners_dict = ""

    if (election.seats):
        seats = pd.read_csv(election.seats)
        seats["constituency_total"] = seats.sum(axis=1)
        seats_dict = seats.to_dict("records")
        seat_winners = pd.read_csv(election.seat_winners)
        seat_winners_dict = seat_winners.to_dict("records")

    context = {
        "page_title": election,
        "election": election,
        "candidates_dict": candidates_dict,
        "candidates_json": candidates_json,
        "results": results,
        "results_dict": results_dict,
        "results_json": results_json,
        "seat_winners_dict": seat_winners_dict,
        "seats_dict": seats_dict,
    }

    if request.GET.get("iframe"):
        return render(request, "iframe.html", context)
    else:
        return render(request, "election.html", context)

def election_embedded(request, slug):
    election = get_object_or_404(Election, slug=slug)

    context = {
        "election": election,
        "header": False,
        "footer": False,
    }

    return render(request, "election-embedded.html", context)