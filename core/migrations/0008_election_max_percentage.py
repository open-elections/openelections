# Generated by Django 2.2.13 on 2020-06-20 12:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0007_auto_20200619_1905'),
    ]

    operations = [
        migrations.AddField(
            model_name='election',
            name='max_percentage',
            field=models.IntegerField(default=85),
            preserve_default=False,
        ),
    ]
