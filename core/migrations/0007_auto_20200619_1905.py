# Generated by Django 2.2.13 on 2020-06-19 19:05

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0006_election_candidates'),
    ]

    operations = [
        migrations.RenameField(
            model_name='election',
            old_name='results',
            new_name='votes',
        ),
    ]
