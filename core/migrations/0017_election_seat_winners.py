# Generated by Django 3.0.8 on 2020-08-10 08:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0016_election_seats'),
    ]

    operations = [
        migrations.AddField(
            model_name='election',
            name='seat_winners',
            field=models.FileField(blank=True, null=True, upload_to=''),
        ),
    ]
