from django.contrib import admin

from . import models

admin.site.register(models.Country)
admin.site.register(models.Map)
admin.site.register(models.Election)
admin.site.register(models.Zoom)