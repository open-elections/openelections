from django import template
from django.template.defaulttags import register

register = template.Library()

# simple multiplication
@register.simple_tag
def multiply(a, b):
    return a * b

# getting value from dictionary by key
@register.filter
def get_item(dictionary, key):
    if dictionary.get(key):
        return dictionary.get(key)
    else:
        return ""

# getting percentage value for votes
@register.filter
def get_item_percent(dictionary, key):
    if dictionary.get(key):
        return dictionary.get(key + "_percent")
    else:
        return ""

@register.filter
def times(number):
    return range(number)