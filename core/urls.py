from django.urls import path
from django.conf import settings
from django.conf.urls.static import static

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('about', views.about, name='about'),
    path('start', views.start, name='start'),
    path('elections', views.elections, name='elections'),
    path('elections/<slug:slug>/', views.election, name='election'),
    path('elections/<slug:slug>/embedded', views.election_embedded, name='election_embedded'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)