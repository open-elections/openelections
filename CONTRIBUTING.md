# Contribute to Open Elections

The easiest way for non-technical people is to add an election. Please [get in touch](mailto:guus@pollito.org) to send the final results.

### Gathering your data

Open Elections can be used to add **any type of election**: national, local, and anything in between. In order to create a new election map you'll need a few things:

1. A CSV file with the candidates names and some other details
2. The election result data, also in CSV format
3. An SVG map of where the election took place
4. Optionally, the constituency winners and some details
5. Optionally, a final CSV file with the number of seats each candidate has won

The format of each of these is quite simple. The easiest way to understand is to take a look at the files for the [candidate data](https://gitlab.com/open-elections/2019-indonesian-legislative-election/-/blob/master/candidates.csv), the [election data](https://gitlab.com/open-elections/2019-indonesian-legislative-election/-/blob/master/votes.csv), and the [SVG map](https://gitlab.com/open-elections/2019-indonesian-legislative-election/-/blob/master/map.svg) for the 2019 Indonesian legislative election. If you want to add the number of seats each candidate has won, take a look at the [seats file](https://gitlab.com/open-elections/2019-greek-legislative-election/-/blob/master/seats.csv) and [seat winners file](https://gitlab.com/open-elections/2019-greek-legislative-election/-/blob/master/seat-winners.csv) of the 2019 Greek legislative election. There are a few things to note for each file.

#### Candidates

The candidates file, `candidates.csv`, is the smallest, but needs most explaining. Each row is a candidate. Each column represents specific type of data and has some requirements / limitations:

|Name|Description|
|--- |--- |
|`party`|A string of the party name|
|`person`|A string of the person's name (optional)|
|`slug`|A unique string of the candidate. Recommended to only contain letters; no spaces, numbers, or special characters|
|`color`|The HEX code of the party's colour|
|`major`|Set to `TRUE` if you want this candidate to be in the table from the start. Generally these are the major parties that have won a certain percentage of total votes or are otherwise prominent
|`winner`|Set to `TRUE` if they were the biggest in any constituency. This will make sure they are included in the legend


**The order of the candidates matters**. The table is generated in the order of this file, so it's recommended you put parties in order of number of votes received.

#### Results

The **results** file, `votes.csv`, is more straight forward. Simply put the candidates' slugs (the same that you used in the candidates file) in the column headers and the constituency names in the row header. The corresponding data is the number of votes each candidate received in each constituency. Note that **you must include all candidates**. The percentages are calculated based on the sum of all votes, so if you leave out candidates these calculations will be wrong.

#### Map

The map, `map.svg`, should be an SVG file of the relevant area with a `path` for each constituency using the constituency name as its `title`. Note that this must match the constituency name used in the results file.

#### Seat winners (optional)

This file, `seat-winners.csv`, is virtually the same as the candidates file, but it's only the `party`, `slug`,  and `color`. Like the candidates files, order matters. In this case, it's recommended to put candidates in order of left to right on the political spectrum. That way the graph showing the division of seats on a national level will be easier to draw conclusions from.

#### Seats (optional)

The seats file, `seats.csv`, is very similar to the results file. It's the candidates' slugs in the column headers and constituency names in the row header with the number of seats in the corresponding cells.

In many countries seats are only counted on a national level, so you can skip the constituency names and have a single row with the country name as a row header and the number of seats for every party.

**Important to note:** the row with country totals is always necessary, even when you include all constituencies. Some countries use bonus seats so simply adding up the seats from all constituencies isn't enough.