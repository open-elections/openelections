# Open Elections

Easily generate beautiful election maps. Open source, free to use.

Live version at [openelections.dev](https://openelections.dev).

## Self-hosting

Open Elections is a fairly simple Python application. It has few requirements and you can be up-and-running soon.

Please note that these instructions are based on a Linux system with `python 3.8`. It's very well possible this will work on other setups, but it might not. If you encounter (and resolve) issues using a different setup, please let me know so I can improve this guide.

What you will definitely need is [`python`](https://www.python.org/), [`django`](https://www.djangoproject.com/), and [`pandas`](https://pandas.pydata.org/), so make sure you have these installed.

1. **Clone or download this repository**

It's the repo for the Open Elections website and will include things you probably don't need. The map generation part of it might be separated from the website repo at some point, but for now you'll have to do this yourself if you want to use in some other way.

2. **Run the server**

After cloning, `cd` to the repo and run `python3 manage.py runserver`. Assuming you have `python3` and `pandas` installed you shouldn't encounter any errors. If you do, please let me know (how you've resolved them) so I can help/improve this guide.

3. **Add an election**

The easiest way to make sure everything is working is to add data that's been proven to work. An election with complete data (including parliamentary seats) is the [2019 Greek legislative election](https://gitlab.com/open-elections/2019-greek-legislative-election), so let's use that one.

To start using this data we first need to create an admin user. Run `python3 manage.py createsuperuser` and follow the steps. Then, head over to [`http://127.0.0.1:8000/admin/`](http://127.0.0.1:8000/admin/) and log in using the details you've put in earlier.

The admin section is quite straight-forward and adding the data is simple. You can start by adding an election straight away, we'll add the other required parts as we go along. So click on add or head to [`http://127.0.0.1:8000/admin/core/election/add/`](http://127.0.0.1:8000/admin/core/election/add/).

The fields are fairly self-explanatory, but some tips/explanations:

- The `slug` field cannot contain spaces or special characters. It will be used as the end of the URL for the election: `http://127.0.0.1:8000/elections/your-slug/`.
- For any dropdown that has no options yet, click on the green plus (+) to add it.
- The `map` field required direct input, so open `map.svg` with a text editor and copy-paste that into the textarea.
- For each of the files, simply add the corresponding CSV file from the repository.
- The max percentage field refers to the highest percentage any candidate has won in any constituency. This is necessary to generate the colour opacity and legend. For the Greek election this number is `50`.
- The zoom field is optional and can be used to create an extra box that's zoomed in on a specific area of the map, generally a highly urban area with small constituencies. For Greece a good use case is the Attica region, using `175 215 75 40` as the `viewBox` value. To understand how this works, please refer to [the MDN `viewBox` documentation page](https://developer.mozilla.org/en-US/docs/Web/SVG/Attribute/viewBox).

After adding the election it will be available at `http://127.0.0.1:8000/elections/your-slug/`, so check it out to make sure everything works properly. It should look the same as [the online version](https://openelections.dev/elections/2019-greek-legislative-election/).

If you want to add an election with your own data the [specific requirements for each file](https://gitlab.com/open-elections/openelections/-/blob/master/CONTRIBUTING.md) will be helpful.